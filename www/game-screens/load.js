/* global game, CocoonJS */

var loadState = {

    init: function () {

        this.input.maxPointers = 1;

        this.scale.pageAlignHorizontally = true;

    },

    preload: function () {

        // Add a progress bar
        var progressBar = game.add.sprite( game.world.centerX, game.world.centerY, 'progressBar' );
        progressBar.anchor.setTo( 0.5, 0.5 );
        game.load.setPreloadSprite( progressBar );

        // Load all assets
        this.load.path = 'assets/';

        game.load.tilemap( 'map1', 'levels/map.json', null, Phaser.Tilemap.TILED_JSON );

        game.load.image( 'tiles', 'images/tiles.png' );
        game.load.image( 'overlay', 'images/overlay.png' );
        game.load.image( 'intro', 'images/intro.png' );
        game.load.image( 'logo', 'images/logo.png' );
        game.load.image( 'font', 'images/font.png' );
        game.load.image( 'share', 'images/share-button.png' );

        // [mobile-input] Load prerequisites for joypad plugin
        game.load.image('compass', 'images/compass_rose.png');
		game.load.image('touch_segment', 'images/touch_segment.png');
		game.load.image('touch', 'images/touch.png');

        game.load.spritesheet( 'confetti', 'images/confetti.png', 8, 8 );
        game.load.spritesheet( 'player', 'images/player_new.png', 64, 64 );
        game.load.spritesheet( 'baby', 'images/baby.png', 48, 60 );
        game.load.spritesheet( 'baby-thoughts', 'images/baby-thoughts.png', 40, 40 );
        game.load.spritesheet( 'tilemap', 'images/tiles.png', 32, 32 );

        //game.load.image( 'enemy', 'images/enemy.png' );

        // Preload banner ads
        if ( game.device.cocoonJS ) {

            CocoonJS.Ad.preloadBanner();
            CocoonJS.Ad.preloadFullScreen();

        }

    },

    create: function() {

        game.state.start( 'menu' );

    }

};